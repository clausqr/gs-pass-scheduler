from pyorbital.orbital import Orbital
from datetime import datetime
import gs
from pprint import pprint

DEBUG = 1

# Hours to predict ahead
n_hours_predict = 24*7

# Enable calendar API by going to
# (short story): https://developers.google.com/calendar/quickstart/python
# (long story): https://console.developers.google.com/projectselector2/apis/dashboard
# and create a calendar in Google Calendar to use with upcoming passes, point cal_name to it.

# Calendar name to sync to:

# Satellite Names:
sat_names = ['BUGSAT-1 (TITA)',
             'BEESAT-4', 'BEESAT-2', 'TECHNOSAT', 'BEESAT 9',
             'MAYA-1', 'UITMSAT-1', 'BHUTAN-1', 'RAAVANA-1', 'NEPALISAT-1', 'UGUISU']
sat_colors = [7,
              11, 11, 11, 11,
              5, 5, 5, 5, 5, 5]

# Ground Station Data
#TODO: Read the groundstations and sat data from database
gs1 = {
    'gs_name': 'ETM',
    'lat_d': -34.5807,
    'lon_d': -58.5232,
    'alt_m': 50,
    'cal_name': 'ETM Passes',
    'min_max_el': 0,
}

gs2 = {
    'gs_name': 'SMT',
    'lat_d': -68.1333,
    'lon_d': -67.0999,
    'alt_m': 10,
    'cal_name': 'SMT Passes',
    'min_max_el': 5,
}

gs_list = [gs1, gs2]




cal_service = gs.cal_connect_service()


now = datetime.utcnow()
start_date = datetime(2019, 8, 25, 0, 0, 0, 0)


for g in gs_list:
    lon = g['lon_d']
    lat = g['lat_d']
    alt = g['alt_m']
    min_max_el = g['min_max_el']
    cal_name = g['cal_name']
    gs_name = g['gs_name']

    cal_id = gs.get_cal_id(cal_service, cal_name)
    print('Processing ' + gs_name + ':')

    for sat_name, sat_color in zip(sat_names, sat_colors):
        orb = Orbital(sat_name)
        next_passes = orb.get_next_passes(start_date, n_hours_predict, lon, lat, alt, tol=0.001, horizon=0)
        print('Processing ' + sat_name + ':')
        for p in next_passes:
            t_start = p[0]
            t_end = p[1]
            t_max_el = p[2]
            [az, max_el] = orb.get_observer_look(t_max_el, lon, lat, alt)

            if max_el < min_max_el:
                print('Discarding pass over ' + gs_name + ' at ' + t_start.isoformat() + ' due to low elevation. (MAX_EL=' + str(int(max_el)) + ')')
            else:
                print('Adding pass over ' + gs_name + ' at ' + t_start.isoformat() + ', MAX_EL = ' + str(int(max_el)))
                n = orb.get_orbit_number(t_max_el, tbus_style=True)  # t bus_style to coincide with gpredict orbit number
                # Event filled according to
                # https://developers.google.com/calendar/v3/reference/events/insert
                event = {
                    'summary': sat_name+' EL='+str(int(max_el)),
                    # 'description': 'MAX.EL.=' + str(int(max_el)),
                    'start': {
                        'dateTime': t_start.isoformat() + 'Z',
                    },
                    'end': {
                        'dateTime': t_end.isoformat() + 'Z',
                    },
                    'colorId': sat_color,
                    'status': 'tentative',
                    'description': 'auto-filled',
                }

                if DEBUG:
                    pprint(event)
                else:
                    gs.insert_event(cal_service, cal_id, event)



